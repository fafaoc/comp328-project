#ifndef __ENGINE
#define __ENGINE


//#include <unistd.h>
#include <assert.h>
#include <stdlib.h>
#include <stdio.h>
#include <math.h>
//#include <string.h>	// C string
#include <cstring>	// C string
#include <string>	// C++ string
#include <iostream>
#include <vector>

using namespace std;



enum ACTION { 	ACT_M=0, ACT_X=1, ACT_O=2, ACT_H=3, 
				ACT_P=4, ACT_K=5, ACT_L=6, ACT_B=7, ACT_S=8, 
				ACT_A=9, ACT_E=10, ACT_G=11, ACT_F=12, ACT_U=13, QUIT=999, DUMMY=-1, END_ROUND=100 };
				
#define ACT_NAMES ("MXOHPKLBSAEGFU")
// make sure ACT_NAMES and ACTION are consistent

#define NUM_CARD_TYPES (14)

#define MAX_HEALTH (9)

#define PRINT_MAP_DELAY ((1*1000000))	// default delay: 1s (= 1000000 micro s)





enum COLOR { BLACK = 30, RED = 31, GREEN = 32, BROWN = 33, BLUE = 34, MAGENTA = 35, CYAN = 36, WHITE = 37 };
enum DIRECTION { D_NORTH, D_EAST, D_SOUTH, D_WEST, D_NIL };
enum TERRAIN { 	T_GRASS='+', T_FOREST='#', T_MOUNT='<', T_DESERT='%', T_SPRING='~'	};
enum TEAM_REF { R_ALLY, R_ENEMY };
enum TEAM_TYPE { TEAM_A='A', TEAM_B='B', TEAM_NIL='N' };

	

struct Point { // point only, no terrain/unit info
	int x,y;
	
	void reset();
	void step(DIRECTION dir);
};


struct Unit {
	int tid,uid;
	bool isAlive;
	int health;
	int attack_times;
	Point pt;
	bool flag_A,flag_E,flag_G,flag_F;
	
	char status(const int CurTeam);
};


struct Cell {
	TERRAIN type; 
	int tid,uid;
};


struct MapInfo {
	Cell** Mapdata;
	Point MapSize; // size for X/Y
	COLOR getTerrainColor(char ctype);
	int getRequiredMoves(char ctype);
	TERRAIN generateTerrain();
	bool isValidCell(Point pt);
	Cell& atCell(Point pt);
	Cell& atCell(Unit& p);
	
	// caution: no checking here
	void enterCell(Unit& p,Point pt_new);
};



class GameEngine;	// class declaration (forward reference)



class Agent {

public:	
	Agent(GameEngine* _game,int _tid);
	int getTID();
	void printPrompt(int CurrentRound);
	bool isAlly(Unit p);
	
	// must use a pure specifier (= 0) for pure virtual function
	virtual void executeRound(int CurrentRound) = 0;
	virtual ACTION dropCard() = 0;
	
protected:
	int tid;
	GameEngine* game;
	
	COLOR mycolor;
	char mylabel;	
};


class GameEngine {

public:
	GameEngine(int _num_team_units,int _map_size_x,int _map_size_y,int _num_cards_round);
	bool isEndGame();
	TEAM_TYPE WinTeam();
	char TeamLabel(int tid);
	COLOR getTeamColor(int tid);
	void quit();
	bool addAgent(Agent* agt);
	bool startGame();
	
	// action method
	void executeAction(ACTION cmd,int param1=-1,int param2=-1);
	
	// information methods
	int countCard(ACTION cmd);
	Point getMapSize();
	TERRAIN getTerrain(Point pt);
	void getAllUnits(TEAM_REF ref,vector<int>& result);	
	Unit getUnitBy(TEAM_REF ref,int uid);
	Unit getUnitAt(Point pt);
	
private:
	MapInfo MyMap;
	Agent* Players[2];
	Unit* TeamUnitInfo[2];
	int NumOfUnits;
	int NumAlive[2];
	int Cards[2][NUM_CARD_TYPES];
	int CurrentTeam;
	int CurrentRound;
	string message;
	
	int NUM_CARD_DRAW,NUM_CARD_KEEP;
	
	bool isValidUnit(int tid,int uid);
	void prepareRound(bool isStart);
	void killUnit(Unit& p);
	void drawCards();
	void printMap(bool hasMessage);
	void discardCards();
	
	bool isHit(Unit& p,Unit& enemy);
	int doAttack(Unit& p,Point target);
	bool __executeAction(ACTION cmd,int param1,int param2);
	
};

				
#endif //__ENGINE

