------------
|README.TXT|
------------

*** This game engine is developed by Dr. Ken Yiu for the COMP328 course project only.
	All rights reserved.  January 2013.

  
1. How to compile this game (at apollo server)?

>	Type "make game"

	
	
2. How to run this game (at apollo server)?

>	Type "./game"

	
	
3. How can I configure the map size, number of units, player agents in this game?

>	Check "game.cpp"
	
	
	
4. What are the main classes used in this game?

>	GameEngine: the game engine
	Agent: the base class for player agent
	HumanAgent: human player agent
	ComAgentGXX: computer player agent
	
	
	
5. Which file(s) should I work on?

>	You just need to work on "ComAgentGXX.cpp".
	If you wish, you may add extra functions/methods in "ComAgentGXX.h" for your own usage.
	
	
	
6. If I find a bug in the game engine, what should I do?

>	You should inform us about the bug, so that we will fix it and release a new version.
	Please don't modify the game engine by yourself.
	

	
7. What are the meanings of the methods in "ComAgentGXX.cpp"?

>	"executeRound" is used to execute actions for the team (in the current round).
	"dropCard" is used to drop a card (at the end of current round)

	

8. What can I do in the "executeRound" method of "ComAgentGXX"?

>	You may call the public methods in the game engine,
		e.g., "game->methodXX(...)"
	especially call the action method: 	"executeAction"
	or call the information methods:  "countCard", "getMapSize", "getTerrain", "getAllUnits", "getUnitBy", "getUnitAt"
	

	
9. What kinds of parameters are used in the "executeAction" and information methods in "GameEngine"?

>	The format of action commands can be found in the project description document.
	The types/values for "ACTION", "DIRECTION", "TERRAIN", ... can be found in "engine.h".
	

	
10. Note that the human player agent ("agent_human.cpp") calls only "executeAction" but not information methods.
	For your reference, you may check the "executeRound" and "acceptInput" methods in "agent_human.cpp"
	to see how they specify the parameters in "executeAction".
