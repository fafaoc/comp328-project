Package CardWarGame;

Helpers
letter = (['a'..'z']|['A'..'Z']);
digit = ['0' .. '9'];

Tokens

number = digit+;
operator_add = '+';
operator_sub = '-';
operator_openbracket = '(';
operator_closebracket = ')';
operator_ruleopen='{';
operator_ruleclose ='}';
operator_semi=';';
operator_dot = '.';
operator_comma = ',';
operator_percent = '%';
operator_largerthan = '>';
operator_smallerthan = '<';
operator_equalto = '=';
keyword_drop = 'DROP';
keyword_nearest = 'Nearest';
keyword_near = 'Near';
keyword_near3x3 = 'Near3x3';
keyword_line = 'Line';
keyword_location_type= 'TerrianType';
keyword_to = 'UnitToUnit';
keyword_not = 'Not';
keyword_target = 'Target';
keyword_hp = 'Hp';
keyword_evasion = 'Er';
keyword_card = 'Card';
keyword_boost = 'Boost';
keyword_count = 'Count';
terrain_grassland = 'Grassland';
terrain_forest = 'Forest';
terrain_mountain = 'Mountain';
terrain_desert = 'Desert';
terrain_spring = 'Spring';
action_move = 'Move';
action_exchange = 'Exchange';
action_occupy = 'Occupy';
action_heal = 'Heal';
action_steal = 'Steal';
action_punch = 'Punch';
action_kick = 'Kick';
action_laser = 'Laser';
action_bomb = 'Bomb';
action_aim = 'Aim';
action_evade = 'Evade';
action_guard = 'Guard';
action_force = 'Force';
action_unlimit = 'Unlimited';
identifier = letter+;
blank = ( ' ' | 13 | 10 | 9 )+;

Ignored Tokens
blank;

Productions

rule_statements = {multi_statement}rule_statement rule_statements  | 
				  {single_statement}rule_statement; 
				  
rule = {fact}fact |{actions_rule}actions_rule
	    ;

rule_statement = rule operator_semi;


//fact part	

fact_keyword = {unit_unit_direction}keyword_to |
	{enemy_nearest}keyword_nearest  |
	{enemy_near}keyword_near |
	{enemy_near3x3}keyword_near3x3 |
	{enemy_line}keyword_line |
	{unit_health}keyword_hp ;
	
	
comparison =	{larger_than} operator_largerthan |
				{smaller_than} operator_smallerthan |
				{equal_to} operator_equalto;	


identifiers = {one_identifier}identifier |{multi_identifier} identifier operator_comma identifiers; 

fact={fact} fact_keyword operator_openbracket identifiers operator_closebracket;

//Action Part
actions_keyword =  {action_heal}action_heal |
				{action_kick}action_kick|
				{action_aim}action_aim |
				{action_evade}action_evade|
				{action_guard}action_guard |
				{action_force}action_force |
				{action_unlimited}action_unlimit |
				{action_move}action_move |
				{action_bomb}action_bomb |
				{action_laser}action_laser |
				{action_punch}action_punch |
				{action_steal}action_steal |
				{action_occupy}action_occupy|
				{action_exchange}action_exchange ;
			
actions_rule ={actions_rule} actions_keyword operator_openbracket identifiers operator_closebracket operator_ruleopen rule_statements operator_ruleclose;





