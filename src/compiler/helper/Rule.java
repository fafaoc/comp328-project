package compiler.helper;

import java.util.ArrayList;
import java.util.Iterator;

public abstract class Rule implements CodeGenerator {
	public ArrayList<Parameter> parameterList;
	public Rule(){
		parameterList= new ArrayList<Parameter> ();
	}
	public abstract void verify() throws Exception;
	
	public void addParameter(Parameter parameter) throws Exception{
		parameterList.add(parameter);
	}
	
	public String codeGenerate(int ruleNo) throws Exception {
		this.verify();
		String ret="\n";
		Iterator<Parameter> it = parameterList.iterator();
		while(it.hasNext()){
			Parameter temp = it.next();
			ret+=temp.codeGenerate(0);
		}

		return ret;
	}
}
