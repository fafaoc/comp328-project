package compiler.helper;

public class Parameter implements CodeGenerator{
	public enum DataType{
	    _Unit,
	    _int,
	    _DIRECTION,
	    _TERRIAN,
		_Comparison
	}
	
	public enum Comparison{
		_equal,
		_bigger,
		_biggerOrEqual,
		_smaller,
		_smallerOrEqual
	}
	
	public enum Team{
		R_ALLY,
		R_ENEMY
	}
	private String value;
	private DataType type;
	private Team team;
	private Comparison comparisonType;
	
	public Parameter(String value){
		this.value=value;
	}
	public void setType(DataType type) throws Exception{
		this.type=type;
		if(type==DataType._Comparison){
			switch(value){
			case ">":
				comparisonType=Comparison._bigger;
				break;
			case ">=":
				comparisonType=Comparison._biggerOrEqual;
				break;
			case "==":
				comparisonType=Comparison._equal;
				break;
			case "<":
				comparisonType=Comparison._smaller;
				break;
			case "<=":
				comparisonType=Comparison._smallerOrEqual;
				break;
			default:
				throw new Exception("Error in comparison token");
			}
		}
	}
	
	public void setTeam(Team team){
		this.team= team;
	}
	@Override
	public String codeGenerate(int ruleNo) {
		String ret="\ttemp->push_back(new Parameter(game,"+type.toString()+",";
		switch(type){
		case _Unit:
			ret+="\""+value+"\","+team.toString();
			break;
		case _int:
			ret+=value;
			break;
		case _DIRECTION:
			ret+="\""+value+"\"";
			break;
		case _Comparison:
			ret+=comparisonType.toString();
			break;
		default:
			break;
		}
		ret+="));\n";
		return ret;
	}

}
