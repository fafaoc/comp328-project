package compiler.helper;

import java.util.ArrayList;
import java.util.Iterator;

public class Program implements CodeGenerator {
	ArrayList<ActionRule> actionRuleList;
	
	public Program(){
		actionRuleList= new ArrayList<ActionRule>();
	}
	
	public void addActionRule(ActionRule a){
		actionRuleList.add(a);
	}
	
	public String header(){
		String ret="#include \"agent_group09.h\"\n"+
					"#include \"ActionRule.h\"\n"+
					"#include \"Rule.h\"\n"+
					"void ComAgentG09::executeRound(int CurrentRound) {\n"+
					"\tvector<Parameter*> * temp =new vector<Parameter *>();\n";
		return ret;
	}
	
	public String footer(){
		String ret="}\n"+
					"ACTION ComAgentG09::dropCard() {\n"+
					"return DUMMY;\n"+
					"}\n";
		return ret;
	}
    

	
	@Override
	public String codeGenerate(int ruleNo) {
		Iterator<ActionRule> it= actionRuleList.iterator();
		int counter=0;
		String ret="";
		ret+=header();
		while(it.hasNext()){
			ActionRule temp= it.next();
			try {
				ret+=temp.codeGenerate(counter);
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		ret+=footer();
		return ret;
	}

}
