package compiler.helper;

import java.util.ArrayList;
import java.util.Iterator;

public abstract class ActionRule implements CodeGenerator {
	protected ArrayList<Parameter> parameterList;
	protected ArrayList<Rule> ruleList; 
	
	public ActionRule(){
		parameterList=new ArrayList<Parameter>();
		ruleList=new ArrayList<Rule>();
	}
	public void addParameter(Parameter parameter) throws Exception{
		parameterList.add(parameter);
	}
	
	public void addRule(Rule rule){
		ruleList.add(rule);
	}
	
	public abstract void verify() throws Exception;
	
	
	@Override
	public String codeGenerate(int ruleNo) throws Exception {
		this.verify();
		String ret="";
		Iterator<Parameter> it = parameterList.iterator();
		while(it.hasNext()){
			Parameter temp = it.next();
			ret+=temp.codeGenerate(0);
		}
		String className=this.getClass().getName();
		className = className.substring(className.indexOf(".")+1);
		
		ret+="\t"+className+"* rule"+ruleNo+"= new "+className+"(game, temp);\n";
		Iterator<Rule> itRule= ruleList.iterator();
		while(itRule.hasNext()){
			ret+="\n\ttemp =new vector<Parameter *>();";
			Rule temp = itRule.next();
			ret+=temp.codeGenerate(0);
			String ruleClassName=temp.getClass().getName();
			ruleClassName=ruleClassName.substring(ruleClassName.indexOf(".")+1);
			ret+="\trule"+ruleNo+"->addRule(new "+ruleClassName+"(game,temp));\n";
		}
		ret+="\trule"+ruleNo+"->execute();\n" +
				"\trule"+ruleNo+"->actionExecution();\n";
		return ret;
	}
} 