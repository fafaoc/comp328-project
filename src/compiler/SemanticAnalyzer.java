package compiler;

import rule.Nearest;
import rule.UnitToUnitDirection;
import actionrule.ActionMove;
import compiler.helper.ActionRule;
import compiler.helper.Parameter;
import compiler.helper.Program;
import compiler.helper.Rule;

import CardWarGame.analysis.DepthFirstAdapter;
import CardWarGame.node.*;

public class SemanticAnalyzer extends DepthFirstAdapter {
	 private Program program;
	 private ActionRule currentActionRule;
	 private Rule currentRule;
	 public void inStart(Start node)
	    {
	        program =new Program();
	    }
	 public void outStart(Start node)
	    {
		 	System.out.println(program.codeGenerate(0));
	    }
	 public void inAOneIdentifierIdentifiers(AOneIdentifierIdentifiers node)
	    {
	        if(currentRule==null){
	        	try {
					currentActionRule.addParameter(new Parameter(node.getIdentifier().getText()));
				} catch (Exception e) {
				}
	        }else{
	        	try {
					currentRule.addParameter(new Parameter(node.getIdentifier().getText()));
				} catch (Exception e) {
				}
	        }		
	    }
	 
	 public void inAMultiIdentifierIdentifiers(AMultiIdentifierIdentifiers node)
	 {
	        if(currentRule==null){
	        	try {
					currentActionRule.addParameter(new Parameter(node.getIdentifier().getText()));
				} catch (Exception e) {
				}
	        }else{
	        	try {
					currentRule.addParameter(new Parameter(node.getIdentifier().getText()));
				} catch (Exception e) {
				}
	        }		
	   }
	 ///////////////////////////////////////////////////////////////////////////////////////////////////////
	 
	  public void inAActionMoveActionsKeyword(AActionMoveActionsKeyword node)
	    {
	        currentActionRule=new ActionMove();
	    }
	  
	  public void outAActionsRuleActionsRule(AActionsRuleActionsRule node){
		  program.addActionRule(currentActionRule);
		  currentActionRule=null;
	  }
	  
	  //////////////////////////////////////////////////////////////////////////////////////////////////////////
	  public void inAUnitUnitDirectionFactKeyword(AUnitUnitDirectionFactKeyword node)
	    {
		  currentRule=new UnitToUnitDirection();
	    }
	  public void inAEnemyNearestFactKeyword(AEnemyNearestFactKeyword node)
	    {
		  currentRule=new Nearest();
	    }
	  public void outAFactFact(AFactFact node)
	    {
		  currentActionRule.addRule(currentRule);
		  currentRule=null;
	    }
	  


}
