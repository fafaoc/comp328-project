/* This file was generated by SableCC (http://www.sablecc.org/). */

package CardWarGame.node;

import CardWarGame.analysis.*;

@SuppressWarnings("nls")
public final class TOperatorLargerthan extends Token
{
    public TOperatorLargerthan()
    {
        super.setText(">");
    }

    public TOperatorLargerthan(int line, int pos)
    {
        super.setText(">");
        setLine(line);
        setPos(pos);
    }

    @Override
    public Object clone()
    {
      return new TOperatorLargerthan(getLine(), getPos());
    }

    @Override
    public void apply(Switch sw)
    {
        ((Analysis) sw).caseTOperatorLargerthan(this);
    }

    @Override
    public void setText(@SuppressWarnings("unused") String text)
    {
        throw new RuntimeException("Cannot change TOperatorLargerthan text.");
    }
}
