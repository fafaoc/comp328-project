/* This file was generated by SableCC (http://www.sablecc.org/). */

package CardWarGame.node;

import CardWarGame.analysis.*;

@SuppressWarnings("nls")
public final class TOperatorRuleopen extends Token
{
    public TOperatorRuleopen()
    {
        super.setText("{");
    }

    public TOperatorRuleopen(int line, int pos)
    {
        super.setText("{");
        setLine(line);
        setPos(pos);
    }

    @Override
    public Object clone()
    {
      return new TOperatorRuleopen(getLine(), getPos());
    }

    @Override
    public void apply(Switch sw)
    {
        ((Analysis) sw).caseTOperatorRuleopen(this);
    }

    @Override
    public void setText(@SuppressWarnings("unused") String text)
    {
        throw new RuntimeException("Cannot change TOperatorRuleopen text.");
    }
}
