/* This file was generated by SableCC (http://www.sablecc.org/). */

package CardWarGame.node;

import CardWarGame.analysis.*;

@SuppressWarnings("nls")
public final class AActionForceActionsKeyword extends PActionsKeyword
{
    private TActionForce _actionForce_;

    public AActionForceActionsKeyword()
    {
        // Constructor
    }

    public AActionForceActionsKeyword(
        @SuppressWarnings("hiding") TActionForce _actionForce_)
    {
        // Constructor
        setActionForce(_actionForce_);

    }

    @Override
    public Object clone()
    {
        return new AActionForceActionsKeyword(
            cloneNode(this._actionForce_));
    }

    @Override
    public void apply(Switch sw)
    {
        ((Analysis) sw).caseAActionForceActionsKeyword(this);
    }

    public TActionForce getActionForce()
    {
        return this._actionForce_;
    }

    public void setActionForce(TActionForce node)
    {
        if(this._actionForce_ != null)
        {
            this._actionForce_.parent(null);
        }

        if(node != null)
        {
            if(node.parent() != null)
            {
                node.parent().removeChild(node);
            }

            node.parent(this);
        }

        this._actionForce_ = node;
    }

    @Override
    public String toString()
    {
        return ""
            + toString(this._actionForce_);
    }

    @Override
    void removeChild(@SuppressWarnings("unused") Node child)
    {
        // Remove child
        if(this._actionForce_ == child)
        {
            this._actionForce_ = null;
            return;
        }

        throw new RuntimeException("Not a child.");
    }

    @Override
    void replaceChild(@SuppressWarnings("unused") Node oldChild, @SuppressWarnings("unused") Node newChild)
    {
        // Replace child
        if(this._actionForce_ == oldChild)
        {
            setActionForce((TActionForce) newChild);
            return;
        }

        throw new RuntimeException("Not a child.");
    }
}
