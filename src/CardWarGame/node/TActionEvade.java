/* This file was generated by SableCC (http://www.sablecc.org/). */

package CardWarGame.node;

import CardWarGame.analysis.*;

@SuppressWarnings("nls")
public final class TActionEvade extends Token
{
    public TActionEvade()
    {
        super.setText("Evade");
    }

    public TActionEvade(int line, int pos)
    {
        super.setText("Evade");
        setLine(line);
        setPos(pos);
    }

    @Override
    public Object clone()
    {
      return new TActionEvade(getLine(), getPos());
    }

    @Override
    public void apply(Switch sw)
    {
        ((Analysis) sw).caseTActionEvade(this);
    }

    @Override
    public void setText(@SuppressWarnings("unused") String text)
    {
        throw new RuntimeException("Cannot change TActionEvade text.");
    }
}
