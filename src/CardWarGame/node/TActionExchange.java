/* This file was generated by SableCC (http://www.sablecc.org/). */

package CardWarGame.node;

import CardWarGame.analysis.*;

@SuppressWarnings("nls")
public final class TActionExchange extends Token
{
    public TActionExchange()
    {
        super.setText("Exchange");
    }

    public TActionExchange(int line, int pos)
    {
        super.setText("Exchange");
        setLine(line);
        setPos(pos);
    }

    @Override
    public Object clone()
    {
      return new TActionExchange(getLine(), getPos());
    }

    @Override
    public void apply(Switch sw)
    {
        ((Analysis) sw).caseTActionExchange(this);
    }

    @Override
    public void setText(@SuppressWarnings("unused") String text)
    {
        throw new RuntimeException("Cannot change TActionExchange text.");
    }
}
