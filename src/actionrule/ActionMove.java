package actionrule;

import java.util.Iterator;

import compiler.helper.ActionRule;
import compiler.helper.Parameter;

public class ActionMove extends ActionRule{
	public void verify() throws Exception{
		Iterator<Parameter> it= parameterList.iterator();
		int counter= 1;
		while(it.hasNext()){
			Parameter temp= it.next();
			switch(counter){
			case 1:
				temp.setType(Parameter.DataType._Unit);
				temp.setTeam(Parameter.Team.R_ALLY);
				break;
			case 2:
				temp.setType(Parameter.DataType._DIRECTION);
				break;
			}
	
		}
	}
}
