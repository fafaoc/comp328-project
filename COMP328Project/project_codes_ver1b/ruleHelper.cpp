//
//  ruleHelper.cpp
//  COMP328Project
//
//  Created by FAFA on 13年5月4日.
//  Copyright (c) 2013年 FAFA. All rights reserved.
//

#include "ruleHelper.h"

Result::Result(){
    table= new vector<unordered_map<string ,DataCell *> *>();
    columnNameList = new vector<char *>;
}
Result::~Result(){
    delete(table);
    delete (columnNameList);
}


void Result:: addColumn(char *  columnName, vector<DataCell *> * data ){
    if(columnNameList->size()==0){
        columnNameList->push_back(columnName);
        for(vector<DataCell *> ::iterator it= data->begin();it!=data->end();it++){
            unordered_map<string ,DataCell *> * tempRow =new unordered_map<string ,DataCell *> ;
            (*tempRow)[columnName]=(*it);
            table->push_back(tempRow);
        }
    }
    else{
        if(!isExistColumn(columnName)){
            columnNameList->push_back(columnName);
            vector<unordered_map<string ,DataCell *> *> * newTable=new vector<unordered_map<string ,DataCell *> *>();
            for(vector<unordered_map<string ,DataCell *> *>::iterator it = table->begin();it!=table->end();){
                for(vector<DataCell *>::iterator dataIt= data->begin();dataIt!=data->end();dataIt++){
                    unordered_map<string ,DataCell *>* tempRow = new unordered_map<string ,DataCell *>;
                    for(vector<char *>::iterator columnIt= columnNameList->begin(); columnIt!=columnNameList->end();columnIt++){
                        (*tempRow)[(*columnIt)] =(**it)[(*columnIt)];
                    }
                    (*tempRow)[columnName]=(*dataIt);
                    newTable->push_back(tempRow);
                }
                it= table->erase(it);
            }
            delete(table);
            table=newTable;
        }
    }
}

bool Result::isExistColumn(char *name){
    for(vector<char *>::iterator iterator = columnNameList->begin();iterator!=columnNameList->end();iterator++){
        if(!strcmp(name, *iterator)){
            return true;
        }
    }
    return false;
}

vector<unordered_map<string ,DataCell *> *> * Result::getExecuteSet(vector<Parameter * > * parameter){
	vector<int> list;
    vector<unordered_map<string ,DataCell *> *> * ret= new vector<unordered_map<string ,DataCell *> *>;
    for(vector<unordered_map<string ,DataCell *> *>::iterator it = table->begin(); it!= table->end(); it++){
        unordered_map<string ,DataCell *> * temprow = new unordered_map<string ,DataCell *>();
		Unit * currentUnit=0;
        for (vector<Parameter *>::iterator parameterIt = parameter->begin(); parameterIt!=parameter->end(); parameterIt++) {
            (*temprow)[(*parameterIt)->getName()] = (**it)[(*parameterIt)->getName()];
			if((**it)[(*parameterIt)->getName()]->getType()==_Unit){
				currentUnit=(**it)[(*parameterIt)->getName()]->getUnit();
			}
        }
		bool actioned=0;
		for(vector<int>::iterator checkerIt=list.begin(); checkerIt!=list.end();checkerIt++){
			if((*checkerIt)==currentUnit->uid){
				actioned=1;
				break;
			}
		}
		if(!actioned || currentUnit->attack_times>2){
			ret->push_back(temprow);
			if(currentUnit->attack_times==1)
				list.push_back(currentUnit->uid);
		}
    }
    return ret;
}
////////////////////////////////////////////////////////////////////////


DataCell::DataCell(Unit * data){
    this->type= _Unit;
    this->unitValue= data;
}
DataCell::DataCell(int data){
    this->type=_int;
    this->intValue= data;
}
DataCell::DataCell(TERRAIN * data){
    this->type=_TERRIAN;
    this->terrianValue= data;
}
DataCell::DataCell(Point * data){
    this->type=_Point;
    this->pointValue=data;
}
DataCell::DataCell(DIRECTION data){
    this->directionValue=data;
    this->type=_DIRECTION;
}
Unit * DataCell::getUnit(){
    return this->unitValue;
}
TERRAIN * DataCell::getTERRAIN(){
    return terrianValue;
}
Point* DataCell::getPoint(){
    return pointValue;
}

DIRECTION DataCell::getDirection(){
    return directionValue;
}

DataType DataCell::getType(){
    return this->type;
}

int DataCell::getResultParam(){
    switch(this->type){
        case _Unit:
            return this->getUnit()->uid;
        case _DIRECTION:
            return (int)this->getDirection();
    }
}



////////////////////////////////////////////////////////////////////////



Parameter::Parameter(GameEngine * engine ,  DataType type , const char * name,TEAM_REF team){
    this->engine= engine;
    this->type= type;
    this->name = new char[20];
    strcpy(this->name, name);
    this->team=team;
}
Parameter::Parameter(GameEngine * engine ,  DataType type , const char * name){
    this->engine= engine;
    this->type= type;
    this->name = new char[20];
    strcpy(this->name, name);
}

Parameter::Parameter(GameEngine * engine , DataType type , Comparison comparisonType){
	this->engine= engine;
    this->type= type;
	this->comparisonType=comparisonType;
}

Parameter::Parameter(GameEngine * engine, DataType , int number){
	this->engine=engine;
    this->type= type;
	this->constNum=number;
}
Comparison Parameter::getComparison(){
	return this->comparisonType;
}

int Parameter::getConstNum(){
	return this->constNum;
}
vector<DataCell *> * Parameter::getInitData(){
    switch(type){
    case _Unit:
        return this->getInitUnitData();
        break;
    case _TERRIAN:
     //  return this->getInitTERRAINData();
        break;
    case _DIRECTION:
        return this->getInitDirectionData();
        break;
    default:
        break;
    return 0;
    }
}

vector<DataCell *> * Parameter::getInitDirectionData(){
    vector<DataCell *> * ret =new vector<DataCell*>();
    ret->push_back(new DataCell((DIRECTION)D_NORTH));
    ret->push_back(new DataCell((DIRECTION)D_EAST));
    ret->push_back(new DataCell((DIRECTION)D_SOUTH));
    ret->push_back(new DataCell((DIRECTION)D_WEST));
    return ret;
}

vector<DataCell *> * Parameter::getInitUnitData(){
    vector<DataCell *> * ret = new vector<DataCell *>();
    vector<int> temp ;
    engine->getAllUnits(team, temp);
    for(vector<int>::iterator it = temp.begin(); it!=temp.end(); it++){
		if(engine->getUnitBy(team, (*it)).isAlive){
			if(team==R_ENEMY || (team==R_ALLY && engine->getUnitBy(team, (*it)).attack_times)){
				Unit * tempUnit= new Unit;
				*tempUnit= engine->getUnitBy(team, *it);
				ret->push_back(new DataCell(tempUnit));
			}
		}
    }
    return ret;
}

char * Parameter::getName(){
    return name;
}

DataType Parameter::getType(){
    return type;
}



////////////////////////////////////////////////////////////////////////

Rule::Rule(GameEngine * engine , vector<Parameter *> * parameter){
    this->parameter= parameter;
    this->engine=engine;
}

void Rule::setResult(Result * ret){
    this->result=ret;
}
////////////////////////////////////////////////////////////////////////

ActionRule::ActionRule(GameEngine * engine , vector<Parameter*> * parameter):Rule(engine, parameter){
    this->result=new Result();
    this->ruleList= new vector<Rule *> ();
}

void ActionRule::addRule(Rule * rule){
    ruleList->push_back(rule);
	rule->setResult(this->result);
}

void ActionRule::execute(){
    for(vector<Parameter *>::iterator it= parameter->begin(); it!=parameter->end();it++){
        result->addColumn((*it)->getName(), (*it)->getInitData());
    }
	for(vector<Rule *> ::iterator it = ruleList->begin();it!=ruleList->end();it++){
		(*it)->execute();
	}
}