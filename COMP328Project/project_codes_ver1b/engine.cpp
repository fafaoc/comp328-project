
#include "engine.h"



using namespace std;

//////////////////////////////////////////////////////////////////////////////////////

void Point::reset() {
	x=-1; y=-1;
}
	
void Point::step(DIRECTION dir) {
	if (dir == D_NORTH)
		y++;
	else if (dir == D_SOUTH)
		y--;
	else if (dir == D_WEST)
		x--;
	else if (dir == D_EAST)
		x++;
}

//////////////////////////////////////////////////////////////////////////////////////

char Unit::status(const int CurTeam) {
	char ch=' ';
	if (tid==CurTeam) {
		if ((flag_A)&&(!flag_F))	ch='+';
		if ((!flag_A)&&(flag_F))	ch='!';
		if ((flag_A)&&(flag_F))		ch='^';
	} else {
		if ((flag_E)&&(!flag_G))	ch='-';
		if ((!flag_E)&&(flag_G))	ch='@';
		if ((flag_E)&&(flag_G))		ch='&';
	}
	return ch;
}

//////////////////////////////////////////////////////////////////////////////////////


COLOR MapInfo::getTerrainColor(char ctype) {
	COLOR color=BLACK;
	if (ctype==T_GRASS)		color=GREEN;
	if (ctype==T_FOREST)	color=GREEN;
	if (ctype==T_MOUNT)		color=WHITE;
	if (ctype==T_DESERT)	color=BROWN;
	if (ctype==T_SPRING)	color=CYAN;	
	return color;
}

int MapInfo::getRequiredMoves(char ctype) {
	int num_moves=1;
	if (ctype==T_FOREST)	num_moves=2;
	if (ctype==T_MOUNT)		num_moves=3;
	return num_moves;
}

TERRAIN MapInfo::generateTerrain() {
	double rnum = drand48();	
	if (rnum<0.5)
		return T_GRASS;
	else if (rnum<0.7)
		return T_FOREST;
	else if (rnum<0.8)
		return T_MOUNT;
	else if (rnum<0.9)
		return T_DESERT;
	else // 1.0
		return T_SPRING;
}

bool MapInfo::isValidCell(Point pt) {
	return ( pt.x>=1 && pt.x<=MapSize.x && pt.y>=1 && pt.y<=MapSize.y );
}

Cell& MapInfo::atCell(Point pt) {
	return (Mapdata[pt.x][pt.y]);
}

Cell& MapInfo::atCell(Unit& p) {
	return (atCell(p.pt));
}

// caution: no checking here
void MapInfo::enterCell(Unit& p,Point pt_new) {
	p.pt = pt_new;		// set unit info.
	Cell& c=atCell(p.pt);
	c.tid = p.tid; // enter new cell
	c.uid = p.uid;		
}


//////////////////////////////////////////////////////////////////////////////////////

Agent::Agent(GameEngine* _game,int _tid) {
	tid=_tid;
	game=_game;
	mycolor=game->getTeamColor(tid);
	mylabel=game->TeamLabel(tid);
}

int Agent::getTID() { 
	return tid; 
};

bool Agent::isAlly(Unit p) {
	return (tid==p.tid);
}

void Agent::printPrompt(int CurrentRound) {
	printf("\033[1;%dm[Round %d]\tTeam %c >\t\033[0m",mycolor,CurrentRound,mylabel);
}


//	ACTION Agent::dropCard();						// NO NEED TO IMPLEMENT
//	void Agent::executeRound(int CurrentRound);		// NO NEED TO IMPLEMENT


//////////////////////////////////////////////////////////////////////////////////////

GameEngine::GameEngine(int _num_team_units,int _map_size_x,int _map_size_y,int _num_cards_round) {
	CurrentTeam=0;
	Players[0]=Players[1]=NULL;
	
	NumOfUnits = _num_team_units;	// number of units per team
	MyMap.MapSize.x = _map_size_x;
	MyMap.MapSize.y = _map_size_y;
	
	_num_cards_round = max(_num_cards_round, 1);
	NUM_CARD_DRAW = NUM_CARD_KEEP = _num_cards_round;
	
	assert((2*NumOfUnits) <= (MyMap.MapSize.x*MyMap.MapSize.y));
	
	MyMap.Mapdata = new Cell*[MyMap.MapSize.x+1];
	for (int x = 1;x<=MyMap.MapSize.x;x++) {
		MyMap.Mapdata[x] = new Cell[MyMap.MapSize.y+1];
		for (int y = 1;y<=MyMap.MapSize.y;y++) {
			MyMap.Mapdata[x][y].tid=-1;
			MyMap.Mapdata[x][y].uid=-1;
			MyMap.Mapdata[x][y].type=MyMap.generateTerrain();
		}
	}
	
	//OUTPUT_ID = new int[NumOfUnits+1];	// store temporary ID output
	for (int tid = 0;tid<2;tid++) {
		// ignore "uid=0"
		NumAlive[tid] = NumOfUnits;
		
		TeamUnitInfo[tid] = new Unit[NumOfUnits+1];
		for (int uid = 1;uid<=NumOfUnits;uid++) {
			Unit& p = TeamUnitInfo[tid][uid];
			p.tid = tid;
			p.uid = uid;
			p.isAlive = true;
			p.health = MAX_HEALTH;
			p.attack_times = 1;
			p.flag_A=false;
			p.flag_E=false;
			p.flag_G=false;
			p.flag_F=false;
			
			//printf("%d %d (%d)\n",team_id,i,unit_id);
			// randomly generate a location
			Point gpt;
			do {
				gpt.x = 1+(rand()%MyMap.MapSize.x);	// range: 1...MyMap.MapSize.x
				gpt.y = 1+(rand()%MyMap.MapSize.y);	// range: 1...MyMap.MapSize.y
				// ensure team members on the same side
				if (tid==0) {
					if (gpt.x>MyMap.MapSize.x/2)
						gpt.x=MyMap.MapSize.x-gpt.x+1;
				} else if (tid==1) {
					if (gpt.x<=MyMap.MapSize.x/2)
						gpt.x=MyMap.MapSize.x-gpt.x+1;
				}
			} while (MyMap.atCell(gpt).tid >= 0);
			
			p.pt=gpt;
			MyMap.atCell(p).tid = tid;
			MyMap.atCell(p).uid = uid;
		}
	}
	
	for (int tid = 0;tid<2;tid++) {
		for (int act = 0;act<NUM_CARD_TYPES;act++)
			Cards[tid][act]=0;
	}
}

bool GameEngine::addAgent(Agent* agt) {
	if (agt==NULL)
		return false;

	int tid=agt->getTID();
	if (tid==0||tid==1) {
		Players[tid]=agt;
		return true;
	} else 
		return false;
}

bool GameEngine::startGame() {
	if (Players[0]==NULL||Players[1]==NULL)
		return false;
	
	CurrentRound=1;
	CurrentTeam=0;
	
	while (!isEndGame()) {
		prepareRound(true); // start round
		drawCards();
		printMap(false);
		Agent* agt=Players[CurrentTeam];
		agt->executeRound(CurrentRound);
		
		if (!isEndGame()) { // not quit yet
			discardCards();			// drop excess card
			prepareRound(false); 	// end round
		}
		
		CurrentTeam=1-CurrentTeam;
		if (CurrentTeam==0)
			CurrentRound++;		
	}
	
	printMap(false);
	printf("\n\n*** Winning team: %c ***\n",WinTeam());
	
	return true;
}

void GameEngine::discardCards() {
	int tid=CurrentTeam;
	int total_cards=0;
	for (int act = 0;act<NUM_CARD_TYPES;act++)
		total_cards+=Cards[tid][act];
		
	if (total_cards<=NUM_CARD_KEEP)
		return;
	
	Agent* agt=Players[tid];
	while (total_cards>NUM_CARD_KEEP) {
	
		ACTION cmd=agt->dropCard();
		
		bool isValid=true;
		if (cmd<0 || cmd>=NUM_CARD_TYPES) // invalid command
			isValid=false;
		else if (Cards[tid][cmd]<=0)	// no cards
			isValid=false;
	
		if (!isValid) {
			for (int act=0;act<NUM_CARD_TYPES;act++)
				if (Cards[tid][act]>0)
					cmd=(ACTION)act;
		}
		
		printf("\t\tdrop card %c\n",ACT_NAMES[cmd]);
		Cards[tid][cmd]--;
		total_cards--;
	}
	printMap(false);	
}

void GameEngine::quit() {
	int tid = CurrentTeam;
	printf("Team %c quit!\n",TeamLabel(tid));
	
	for (int uid = 1;uid<=NumOfUnits;uid++) {
		Unit& p = TeamUnitInfo[tid][uid];
		if (p.isAlive) {
			p.health=0;
			killUnit(p);
		}
	}
}

int GameEngine::countCard(ACTION cmd) {
	if (cmd<0 || cmd>=NUM_CARD_TYPES) // invalid command
		return 0;
	else 
		return Cards[CurrentTeam][cmd];
}
	
Point GameEngine::getMapSize() {
	Point pt=MyMap.MapSize;
	return pt;
}

TERRAIN GameEngine::getTerrain(Point pt) {
	if (MyMap.isValidCell(pt))
		return MyMap.atCell(pt).type;
	else
		return T_GRASS;
}

void GameEngine::getAllUnits(TEAM_REF ref,vector<int>& result) {
	result.clear();
	
	int tid=CurrentTeam;
	
	if (ref==R_ENEMY)
		tid=1-CurrentTeam;
	else if (ref!=R_ALLY)	// invalid parameter
		return;
	
	for (int uid = 1;uid<=NumOfUnits;uid++) {
		Unit& p = TeamUnitInfo[tid][uid];
		if (p.isAlive)
			result.push_back(uid);
	}
}

Unit GameEngine::getUnitBy(TEAM_REF ref,int uid) {
	Unit p;
	p.tid=-1;
	p.uid=-1;
	p.isAlive=false;
	
	int tid=-1;
	if (ref==R_ALLY)
		tid=CurrentTeam;
	else if (ref==R_ENEMY)
		tid=1-CurrentTeam;
	
	if (isValidUnit(tid,uid))
		p = TeamUnitInfo[tid][uid];	// copy unit info.
	
	return p;
}

Unit GameEngine::getUnitAt(Point pt) {
	Unit p;
	p.tid=-1;
	p.uid=-1;
	p.isAlive=false;
	
	if (MyMap.isValidCell(pt)) {
		int tid=MyMap.atCell(pt).tid;
		int uid=MyMap.atCell(pt).uid;
		p=TeamUnitInfo[tid][uid];	// copy unit info.
	}
	
	return p;
}

bool GameEngine::isEndGame() {
	return (NumAlive[0]==0 || NumAlive[1]==0);
}

TEAM_TYPE GameEngine::WinTeam() {
	if (NumAlive[0]==0)
		return TEAM_B;
	else if (NumAlive[1]==0)
		return TEAM_A;
	else
		return TEAM_NIL;
} 

char GameEngine::TeamLabel(int tid) {
	if (tid==0)
		return TEAM_A;
	else if (tid==1)
		return TEAM_B;
	else
		return TEAM_NIL;
}

COLOR GameEngine::getTeamColor(int tid) {
	if (tid==0)
		return CYAN;
	else
		return RED;
}

// caution: make this function inaccessible to player agents
void GameEngine::killUnit(Unit& p) {
	if (p.isAlive && p.health<=0) {	
		p.isAlive=false;
		NumAlive[p.tid]--;	// important
		MyMap.atCell(p).tid=-1;
		MyMap.atCell(p).uid=-1;
		p.pt.reset();
	}
}

void GameEngine::drawCards() {
	int tid=CurrentTeam;
	for (int iter=0;iter<NUM_CARD_DRAW;iter++) {
		int cid=rand()%NUM_CARD_TYPES;	// random card
		
		double rnum = drand48();	
		if (rnum<0.3)
			cid=ACT_M;
		else if (rnum<0.4)
			cid=ACT_P;
			
		Cards[tid][cid]++;
	}
}

void GameEngine::printMap(bool hasMessage) {
	char strline[20];
	char displayline[20];

	COLOR color=WHITE;
	
	printf("\n");
	Players[CurrentTeam]->printPrompt(CurrentRound);
	printf("\n");
	for (int y = MyMap.MapSize.y;y>=0;y--) {
	
		for (int lineno = 0;lineno<3;lineno++) {
		
			for (int x = 0;x<=MyMap.MapSize.x;x++) {
				Point pt;
				pt.x=x; 	pt.y=y;
				bool isBright=true;	
				color=BROWN;	// initialize color
				sprintf(strline,"");
				if (lineno == 1) 
					if (x == 0 && y >= 1)
						sprintf(strline,"{%d}",y);
				if (lineno == 0)
					if (y == 0 && x >= 1)
						sprintf(strline,"{%d}",x);
				
				if (x >= 1&&y >= 1) {				
					// default
					char ctype=MyMap.atCell(pt).type;
					color=MyMap.getTerrainColor(ctype);
					isBright=false;
					
					if (lineno!=1)
						sprintf(strline,"%c %c %c",ctype,ctype,ctype);
					else
						sprintf(strline," %c %c ",ctype,ctype);
				
					int uid = MyMap.atCell(pt).uid;
					int tid = MyMap.atCell(pt).tid;
					if (tid >= 0) {
						Unit& p = TeamUnitInfo[tid][uid];
						if (lineno == 1) {
							if (p.isAlive) {
								char status=p.status(CurrentTeam);
								sprintf(strline,"%c%c%d:%d",status,TeamLabel(tid),uid,p.health);
								color=getTeamColor(tid);
								isBright=true;
							}
						}
					}
				}
					
				sprintf(displayline,"%6s",strline);				
				//printf("%s",displayline);
	
				int bcode=(isBright)?(1):(9);
				printf("\033[%d;%dm%s\033[0m",bcode,color,displayline);
					//std::cout << "\033[9;" << color << "m" << displayline << "\033[0m";
			}
			printf("\n");
			if (y==0)
				break;
		}
	}
	printf("\tTeam A: %d \tvs.\t Team B: %d\n\n",NumAlive[0],NumAlive[1]);
	
	
	if (hasMessage)
		if (message.size()>0) { // print message
			printf("\033[1;%dmMessage: %s\033[0m\n\n",GREEN,message.c_str());
			message.clear();
		}	
		
	
	// print cards
	int tid=CurrentTeam;
	int total_cards=0;
	for (int act = 0;act<NUM_CARD_TYPES;act++)
		total_cards+=Cards[tid][act];
	
	printf("Team %c cards(%d):",TeamLabel(tid),total_cards);
	for (int act = 0;act<NUM_CARD_TYPES;act++)
		if (Cards[tid][act]>0)
			printf(" %c(%d)",ACT_NAMES[act],Cards[tid][act]);
	printf("\n\n");
	usleep(PRINT_MAP_DELAY);
}




bool GameEngine::isHit(Unit& p,Unit& enemy) {
	if (p.tid==enemy.tid)	// cannot attack ally
		return false;
	
	// check enemy terrain
	double evade=0.3;	// default evasion rate
	TERRAIN ctype=MyMap.atCell(enemy).type; 	
	if (ctype==T_FOREST)	evade=0.5;
	if (ctype==T_MOUNT)		evade=0.7;
	
	// check flags
	if (p.flag_A)		evade=evade/2.0;
	if (enemy.flag_E)	evade=evade*2.0;
	
	double rnum = drand48();	
	return (rnum>=evade);
}

bool GameEngine::isValidUnit(int tid,int uid) {
	if (tid<0 || tid>1)				// invalid tid
		return false;
	if (uid<1 || uid>NumOfUnits)	// invalid uid
		return false;
	return (TeamUnitInfo[tid][uid].isAlive); // still alive
}

int GameEngine::doAttack(Unit& p,Point target) {
	int etid = MyMap.atCell(target).tid;
	int euid = MyMap.atCell(target).uid;
	
	if (p.tid==etid)	// cannot attack ally
		return 0;
	if (isValidUnit(etid,euid)==false) // alive unit?
		return 0;
	Unit& enemy = TeamUnitInfo[etid][euid]; // identify unit
	
	
	
	char info[50];
	
	int damage=0;
	if (isHit(p,enemy)) {
		damage=2;	// default damage
		// check flags
		if (p.flag_F)		damage=damage*2;
		if (enemy.flag_G)	damage=damage/2;
	
		enemy.health-=damage;
		if (enemy.health<=0)
			killUnit(enemy);
	}
	
	sprintf(info,"     %c%d ",TeamLabel(enemy.tid),enemy.uid);
	message+=info;
	if (damage>0)
		sprintf(info,"hurt %d",damage);
	else
		sprintf(info,"miss");
	message+=info;
	
	return damage;
}

// private call
bool GameEngine::__executeAction(ACTION cmd,int param1,int param2) {
	int atid=CurrentTeam;	// ally team id
	int etid=1-CurrentTeam; // enemy team id
	int uid=param1;
	
	if (cmd<0 || cmd>=NUM_CARD_TYPES) // invalid command
		return false;
	if (Cards[atid][cmd]==0)	// no cards
		return false;
	
	if (isValidUnit(atid,uid)==false)	// invalid uid
		return false;
	Unit& ally = TeamUnitInfo[atid][uid];
	
	
	DIRECTION dir=D_NIL;
	if (param2==D_NORTH||param2==D_EAST||param2==D_SOUTH||param2==D_WEST)
		dir=(DIRECTION)param2;
	Point ptnext=ally.pt;
	ptnext.step(dir);
	
	// is neighbor position valid
	if ( MyMap.isValidCell(ptnext)==false )
		return false;
	

	int next_tid = MyMap.atCell(ptnext).tid;
	int next_uid = MyMap.atCell(ptnext).uid;
	
	if (cmd==ACT_M) { // MOVE
		if (next_tid >= 0)	// already somebody there
			return false;
			
		int num_moves=MyMap.getRequiredMoves(MyMap.atCell(ptnext).type);
		if (Cards[atid][cmd]<num_moves)	// no cards
			return false;
		Cards[atid][cmd]-=num_moves;	// X cards for MOVE
			
		MyMap.atCell(ally).tid = -1;	// leave old cell
		MyMap.atCell(ally).uid = -1;
		MyMap.enterCell(ally,ptnext);	// enter new cell
		return true;
	}
	
	
	Cards[atid][cmd]--;	// 1 card for any other action
	
	
	// HEAL and Boost actions
	if (cmd==ACT_H||cmd==ACT_A||cmd==ACT_E||cmd==ACT_G||cmd==ACT_F||cmd==ACT_U) {
		if (cmd==ACT_H) ally.health = min(ally.health+3,MAX_HEALTH);
		if (cmd==ACT_A) ally.flag_A = true;
		if (cmd==ACT_E) ally.flag_E = true;
		if (cmd==ACT_G) ally.flag_G = true;
		if (cmd==ACT_F) ally.flag_F = true;
		if (cmd==ACT_U) ally.attack_times += 99;		
		return true;
	}
	
	if (cmd==ACT_X) { // EXCHANGE
		int alt_uid=param2;
		if (isValidUnit(atid,alt_uid)==false)
			return false;
		Unit& alt_p = TeamUnitInfo[atid][alt_uid];
		
		Point pt1=ally.pt;
		Point pt2=alt_p.pt;
		MyMap.enterCell(ally,pt2);		// enter new cell
		MyMap.enterCell(alt_p,pt1);		// enter new cell
		return true;
	}
	
	// remaining part for attacks
	
	
	// 1. check turns
	if (ally.attack_times<=0) {
		Cards[atid][cmd]++;		// recover the card
		return false;
	}
	ally.attack_times--;	
	
	// single enemy attack
	if (cmd==ACT_P||cmd==ACT_O||cmd==ACT_S) {
		if ((dir==D_NIL)||(isValidUnit(next_tid,next_uid)==false)) {
			Cards[atid][cmd]++;		// recover the card and attack times
			ally.attack_times++;
			return false;
		}
		Unit& enemy = TeamUnitInfo[next_tid][next_uid];
		
		// differentiate OCCUPY from others
		if (cmd==ACT_O) {
			if (isHit(ally,enemy)) { // consider "enemy" only
				Point pt1=ally.pt;
				Point pt2=enemy.pt;
				MyMap.enterCell(ally,pt2);		// enter new cell
				MyMap.enterCell(enemy,pt1);	// enter new cell
				message+="     hit";
			} else
				message+="     miss";
		}
		if (cmd==ACT_S) {
			int damage=doAttack(ally,ptnext);
			ally.health = min(ally.health+damage,MAX_HEALTH);
		}
		if (cmd==ACT_P) {
			doAttack(ally,ptnext);
		}
		return true;
	}
	
	
	// multiple enemy attack
	
	if (cmd==ACT_K) { // kick
		Point ptN,ptE,ptS,ptW;
		ptN=ptE=ptS=ptW=ally.pt;
		ptN.step(D_NORTH);	doAttack(ally,ptN);
		ptE.step(D_EAST);	doAttack(ally,ptE);
		ptS.step(D_SOUTH);	doAttack(ally,ptS);
		ptW.step(D_WEST);	doAttack(ally,ptW);
	}
	
	if (cmd==ACT_L) { // laser
		Point ptcur=ally.pt;
		ptcur.step(dir);
		while (MyMap.isValidCell(ptcur)) {
			doAttack(ally,ptcur);
			ptcur.step(dir);
		}
	}
	
	if (cmd==ACT_B) { // bomb
		Point ptcenter=ally.pt;
		ptcenter.step(dir);
		ptcenter.step(dir);	// go 2 steps
		for (int dx=-1;dx<=1;dx++) {
			for (int dy=-1;dy<=1;dy++) {
				Point ptcur=ptcenter;
				ptcur.x+=dx;
				ptcur.y+=dy;
				if (MyMap.isValidCell(ptcur))
					doAttack(ally,ptcur);
			}
		}
	}
	
	return true;
}

void GameEngine::executeAction(ACTION cmd,int param1,int param2) {
	//call __executeAction
	bool isDone=__executeAction(cmd,param1,param2);
	
	if (isDone)
		message+="\t[OK]";
	else 
		message+="\t[FAIL]";
	
	printMap(true);
}


void GameEngine::prepareRound(bool isStart) {
	// isStart true: start the round for current player
	// isStart false: end the round for current player
	
	int tid = CurrentTeam;
		
	for (int uid = 1;uid<=NumOfUnits;uid++) {
		Unit& p = TeamUnitInfo[tid][uid];
		if (p.isAlive==false)
			continue;
		
		if (isStart) { // reset status
			p.attack_times = 1;	
			p.flag_A = false;
			p.flag_E = false;
			p.flag_G = false;
			p.flag_F = false;
		} else {	// end round update
			TERRAIN ctype=MyMap.atCell(p).type;
			if (ctype==T_SPRING) { // increment health
				if (p.health<MAX_HEALTH)
					p.health++;
			}
			if (ctype==T_DESERT) { // decrement health
				p.health--;
				if (p.health<=0)
					killUnit(p);
			}
		}
	}
}


//for (int color=30;color<50;color++)
//	std::cout << "\033[9;" << color << "m" << color << "\033[0m" << " ";
	

	