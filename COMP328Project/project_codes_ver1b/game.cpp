
/*
Version 1			Feb 4, 2013

Version 1.1			March 29, 2013
	+ Fix some bugs in actions
	+ Can set the number of cards per round in game.cpp (for your testing purpose) 
	+ Manual player: enter '*' to drop cards automatically in the DROP phase
*/

#include "engine.h"
#include "agent_human.h"
#include "agent_group09.h"

using namespace std;



int main(int argc, char *argv[]){
	int _num_team_units = 8;
	int _map_size_x = 8;
	int _map_size_y = 5;
	
	
	int _num_cards_round = 10;
	// default value: 10
	// for testing purpose, set it to 1000
	
	int random_seed = 0;

	
	srand(random_seed);
	srand48(random_seed);
	
	
	// create a game
	GameEngine* game=new GameEngine(_num_team_units,_map_size_x,_map_size_y,_num_cards_round);
	
	
	const int TeamA_ID=0;
	const int TeamB_ID=1;
	
	Agent* agentA=NULL;
	Agent* agentB=NULL;
	
	agentA=new ComAgentG09(game,TeamA_ID);				// use human player
	//agentA=new ComAgentGXX(game,TeamA_ID);		// use computer player
	
	//agentB=new HumanAgent(game,TeamB_ID);			// use human player
	agentB=new ComAgentG09(game,TeamB_ID);		// use computer player
	
	
	game->addAgent(agentA);
	game->addAgent(agentB);
	game->startGame();

    return 0;
}
