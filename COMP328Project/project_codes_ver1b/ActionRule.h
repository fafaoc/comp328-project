//
//  ActionRule.h
//  COMP328Project
//
//  Created by FAFA on 13年5月5日.
//  Copyright (c) 2013年 FAFA. All rights reserved.
//

#ifndef __COMP328Project__ActionRule__
#define __COMP328Project__ActionRule__

#include <iostream>
#include "ruleHelper.h"

class ActionMove: public ActionRule{
public:
    ActionMove(GameEngine * game, vector<Parameter *> * parameter): ActionRule(game, parameter){}
    void actionExecution();
};

class ActionHeal: public ActionRule{
public:
    ActionHeal(GameEngine * game, vector<Parameter *> * parameter): ActionRule(game, parameter){}
    void actionExecution();
};

class ActionKick: public ActionRule{
public:
    ActionKick(GameEngine * game, vector<Parameter *> * parameter): ActionRule(game, parameter){}
    void actionExecution();
};

class ActionAim: public ActionRule{
public:
    ActionAim(GameEngine * game, vector<Parameter *> * parameter): ActionRule(game, parameter){}
    void actionExecution();
};

class ActionEvade: public ActionRule{
public:
    ActionEvade(GameEngine * game, vector<Parameter *> * parameter): ActionRule(game, parameter){}
    void actionExecution();
};

class ActionGuard: public ActionRule{
public:
    ActionGuard(GameEngine * game, vector<Parameter *> * parameter): ActionRule(game, parameter){}
    void actionExecution();
};

class ActionForce: public ActionRule{
public:
    ActionForce(GameEngine * game, vector<Parameter *> * parameter): ActionRule(game, parameter){}
    void actionExecution();
};

class ActionUnlimit: public ActionRule{
public:
    ActionUnlimit(GameEngine * game, vector<Parameter *> * parameter): ActionRule(game, parameter){}
    void actionExecution();
};

class ActionBomb: public ActionRule{
public:
    ActionBomb(GameEngine * game, vector<Parameter *> * parameter): ActionRule(game, parameter){}
    void actionExecution();
};

class ActionLaser: public ActionRule{
public:
    ActionLaser(GameEngine * game, vector<Parameter *> * parameter): ActionRule(game, parameter){}
    void actionExecution();
};

class ActionPunch: public ActionRule{
public:
    ActionPunch(GameEngine * game, vector<Parameter *> * parameter): ActionRule(game, parameter){}
    void actionExecution();
};

class ActionSteal: public ActionRule{
public:
    ActionSteal(GameEngine * game, vector<Parameter *> * parameter): ActionRule(game, parameter){}
    void actionExecution();
};

class ActionOccupy: public ActionRule{
public:
    ActionOccupy(GameEngine * game, vector<Parameter *> * parameter): ActionRule(game, parameter){}
    void actionExecution();
};

class ActionExchange: public ActionRule{
public:
    ActionExchange(GameEngine * game, vector<Parameter *> * parameter): ActionRule(game, parameter){}
    void actionExecution();
};





#endif /* defined(__COMP328Project__ActionRule__) */
