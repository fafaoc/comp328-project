//
//  ruleHelper.h
//  COMP328Project
//
//  Created by FAFA on 13年5月4日.
//  Copyright (c) 2013年 FAFA. All rights reserved.
//

#ifndef COMP328Project_ruleHelper_h
#define COMP328Project_ruleHelper_h
#include <string>
#include <vector>
#include "engine.h"
#include <unordered_map>

using namespace std;
class DataCell;
struct charCmp;
struct Hash_Func;


enum DataType{
    _Unit,
    _int,
    _Point,
    _DIRECTION,
    _TERRIAN,
	_Comparison
};

enum Comparison{
	_equal,
	_bigger,
	_biggerOrEqual,
	_smaller,
	_smallerOrEqual
};

class Result;

class Parameter{
private:
    GameEngine *engine;
    char * name;
    DataType type;
    TEAM_REF team;
	Comparison comparisonType;
	int constNum;
    
public :
    Parameter(GameEngine * , DataType , const char * ,TEAM_REF );
    Parameter(GameEngine * , DataType , const char *  );
	Parameter(GameEngine * , DataType , Comparison comparisonType);
	Parameter(GameEngine * , DataType , int);
    char * getName();
    DataType getType();
    vector<DataCell *> *getInitData();
    vector<DataCell *> *getInitUnitData();
    //vector<DataCell *> *getInitTERRAINData();
    //vector<DataCell *> *getInitPointData();
    vector<DataCell *> *getInitDirectionData();
	Comparison getComparison();
	int getConstNum();
};

class DataCell{
   
private:

    DataType type;
    int intValue;
    Unit * unitValue;
    TERRAIN * terrianValue;
    Point * pointValue;
    DIRECTION directionValue;
    
public:
    DataCell(Unit * data);
    DataCell(int data);
    DataCell(TERRAIN * data);
    DataCell(Point * data);
    DataCell(DIRECTION data);
    DataCell(DataCell * data);
    DataType getType();
    int getInt();
    Unit * getUnit();
    TERRAIN * getTERRAIN();
    Point* getPoint();
    DIRECTION getDirection();
    int getResultParam();
};

class Rule{
protected:
    GameEngine * engine;
    Result * result;
    vector<Parameter *> * parameter;
public:
    Rule(GameEngine * , vector<Parameter *> * parameter);\
    void setResult(Result * ret);
    virtual void execute()=0;
};


class Result{
private:
    vector<char *> * columnNameList;
    vector<unordered_map<string ,DataCell *> *> * table;
public:
    Result();
    void addColumn(char * columnName, vector<DataCell *> * data );
    void addRows();
    bool isExistColumn(char * name);
    vector<unordered_map<string ,DataCell *> *> * getExecuteSet(vector<Parameter * > * parameter);
    vector<unordered_map<string ,DataCell *> *> * getTable(){
        return table;
    }
    ~Result();
};



class ActionRule : public Rule{
private:
    vector<Rule *> * ruleList;
public:
    ActionRule(GameEngine * engine , vector<Parameter *> * parameter);
    void addRule(Rule * rule);
    void execute();

};

#endif
