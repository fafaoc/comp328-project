
//
//  ActionRule.cpp
//  COMP328Project
//
//  Created by FAFA on 13年5月5日.
//  Copyright (c) 2013年 FAFA. All rights reserved.
//

#include "ActionRule.h"


void ActionMove::actionExecution(){
    vector<unordered_map<string ,DataCell *> *> * temp = this->result->getExecuteSet(this->parameter);
    for(vector<unordered_map<string ,DataCell *> *>::iterator it= temp->begin();it!=temp->end(); it++){
        int param[2];
        int counter=0;
        for(vector<Parameter *>::iterator paramIt = this->parameter->begin(); paramIt!= this->parameter->end(); paramIt++){
            param[counter] = (**it)[(*paramIt)->getName()]->getResultParam();
			counter++;
        }
		if(engine->countCard(ACT_M))
			engine->executeAction(ACT_M,param[0], param[1]);
    }
}


void ActionHeal::actionExecution(){
    vector<unordered_map<string ,DataCell *> *> * temp = this->result->getExecuteSet(this->parameter);
    for(vector<unordered_map<string ,DataCell *> *>::iterator it= temp->begin();it!=temp->end(); it++){
        int param[2];
        int counter=0;
        for(vector<Parameter *>::iterator paramIt = this->parameter->begin(); paramIt!= this->parameter->end(); paramIt++){
            param[counter] = (**it)[(*paramIt)->getName()]->getResultParam();
        }
		if(engine->countCard(ACT_H))
			engine->executeAction(ACT_H,param[0]);
    }
}

void ActionKick::actionExecution(){
    vector<unordered_map<string ,DataCell *> *> * temp = this->result->getExecuteSet(this->parameter);
    for(vector<unordered_map<string ,DataCell *> *>::iterator it= temp->begin();it!=temp->end(); it++){
        int param[2];
        int counter=0;
        for(vector<Parameter *>::iterator paramIt = this->parameter->begin(); paramIt!= this->parameter->end(); paramIt++){
            param[counter] = (**it)[(*paramIt)->getName()]->getResultParam();
        }
		if(engine->countCard(ACT_K))
			engine->executeAction(ACT_K,param[0]);
    }
}

void ActionAim::actionExecution(){
    vector<unordered_map<string ,DataCell *> *> * temp = this->result->getExecuteSet(this->parameter);
    for(vector<unordered_map<string ,DataCell *> *>::iterator it= temp->begin();it!=temp->end(); it++){
        int param[2];
        int counter=0;
        for(vector<Parameter *>::iterator paramIt = this->parameter->begin(); paramIt!= this->parameter->end(); paramIt++){
            param[counter] = (**it)[(*paramIt)->getName()]->getResultParam();
        }
		if(engine->countCard(ACT_A))
			engine->executeAction(ACT_A,param[0]);
    }
}

void ActionGuard::actionExecution(){
    vector<unordered_map<string ,DataCell *> *> * temp = this->result->getExecuteSet(this->parameter);
    for(vector<unordered_map<string ,DataCell *> *>::iterator it= temp->begin();it!=temp->end(); it++){
        int param[2];
        int counter=0;
        for(vector<Parameter *>::iterator paramIt = this->parameter->begin(); paramIt!= this->parameter->end(); paramIt++){
            param[counter] = (**it)[(*paramIt)->getName()]->getResultParam();
        }
		if(engine->countCard(ACT_G))
			engine->executeAction(ACT_G,param[0]);
    }
}

void ActionForce::actionExecution(){
    vector<unordered_map<string ,DataCell *> *> * temp = this->result->getExecuteSet(this->parameter);
    for(vector<unordered_map<string ,DataCell *> *>::iterator it= temp->begin();it!=temp->end(); it++){
        int param[2];
        int counter=0;
        for(vector<Parameter *>::iterator paramIt = this->parameter->begin(); paramIt!= this->parameter->end(); paramIt++){
            param[counter] = (**it)[(*paramIt)->getName()]->getResultParam();
        }
		if(engine->countCard(ACT_F))
			engine->executeAction(ACT_F,param[0]);
    }
}

void ActionUnlimit::actionExecution(){
    vector<unordered_map<string ,DataCell *> *> * temp = this->result->getExecuteSet(this->parameter);
    for(vector<unordered_map<string ,DataCell *> *>::iterator it= temp->begin();it!=temp->end(); it++){
        int param[2];
        int counter=0;
        for(vector<Parameter *>::iterator paramIt = this->parameter->begin(); paramIt!= this->parameter->end(); paramIt++){
            param[counter] = (**it)[(*paramIt)->getName()]->getResultParam();
        }
		if(engine->countCard(ACT_U))
			engine->executeAction(ACT_U,param[0]);
    }
}

void ActionBomb::actionExecution(){
    vector<unordered_map<string ,DataCell *> *> * temp = this->result->getExecuteSet(this->parameter);
    for(vector<unordered_map<string ,DataCell *> *>::iterator it= temp->begin();it!=temp->end(); it++){
        int param[2];
        int counter=0;
        for(vector<Parameter *>::iterator paramIt = this->parameter->begin(); paramIt!= this->parameter->end(); paramIt++){
            param[counter] = (**it)[(*paramIt)->getName()]->getResultParam();
        }
        if(engine->countCard(ACT_B))
			engine->executeAction(ACT_B,param[0],param[1]);
    }
}

void ActionLaser::actionExecution(){
    vector<unordered_map<string ,DataCell *> *> * temp = this->result->getExecuteSet(this->parameter);
    for(vector<unordered_map<string ,DataCell *> *>::iterator it= temp->begin();it!=temp->end(); it++){
        int param[2];
        int counter=0;
        for(vector<Parameter *>::iterator paramIt = this->parameter->begin(); paramIt!= this->parameter->end(); paramIt++){
            param[counter] = (**it)[(*paramIt)->getName()]->getResultParam();
        }
		if(engine->countCard(ACT_L))
			engine->executeAction(ACT_L,param[0],param[1]);
    }
}

void ActionPunch::actionExecution(){
    vector<unordered_map<string ,DataCell *> *> * temp = this->result->getExecuteSet(this->parameter);
    for(vector<unordered_map<string ,DataCell *> *>::iterator it= temp->begin();it!=temp->end(); it++){
        int param[2];
        int counter=0;
        for(vector<Parameter *>::iterator paramIt = this->parameter->begin(); paramIt!= this->parameter->end(); paramIt++){
            param[counter] = (**it)[(*paramIt)->getName()]->getResultParam();
        }
        if(engine->countCard(ACT_P))
			engine->executeAction(ACT_P,param[0],param[1]);
    }
}

void ActionSteal::actionExecution(){
    vector<unordered_map<string ,DataCell *> *> * temp = this->result->getExecuteSet(this->parameter);
    for(vector<unordered_map<string ,DataCell *> *>::iterator it= temp->begin();it!=temp->end(); it++){
        int param[2];
        int counter=0;
        for(vector<Parameter *>::iterator paramIt = this->parameter->begin(); paramIt!= this->parameter->end(); paramIt++){
            param[counter] = (**it)[(*paramIt)->getName()]->getResultParam();
        }
		if(engine->countCard(ACT_S))
			engine->executeAction(ACT_S,param[0],param[1]);
    }
}

void ActionExchange::actionExecution(){
    vector<unordered_map<string ,DataCell *> *> * temp = this->result->getExecuteSet(this->parameter);
    for(vector<unordered_map<string ,DataCell *> *>::iterator it= temp->begin();it!=temp->end(); it++){
        int param[2];
        int counter=0;
        for(vector<Parameter *>::iterator paramIt = this->parameter->begin(); paramIt!= this->parameter->end(); paramIt++){
            param[counter] = (**it)[(*paramIt)->getName()]->getResultParam();
        }
		if(engine->countCard(ACT_X))
			
			engine->executeAction(ACT_X,param[0],param[1]);
    }
}



