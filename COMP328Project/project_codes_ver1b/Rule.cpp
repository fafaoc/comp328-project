//
//  Rule.cpp
//  COMP328Project
//
//  Created by FAFA on 13年5月7日.
//  Copyright (c) 2013年 FAFA. All rights reserved.
//

#include "Rule.h"
bool Near::nearPoint(Point a, Point b){
    if(a.y==b.y) {
        if(b.x==a.x+1 || b.x == a.x-1)
            return true;
    }
    if(a.x==b.x) {
        if(b.y==a.y+1 || b.y == a.y-1)
            return true;
    }
    return false;
}
void Near::execute(){
    for(vector<Parameter *>::iterator it= parameter->begin(); it!=parameter->end();){
        result->addColumn((*it)->getName(), (*it)->getInitData());
    };
    vector<unordered_map<string ,DataCell *> *> * resultTable= result->getTable();
    for(vector<unordered_map<string ,DataCell *> *>::iterator it= resultTable->begin();it!=resultTable->end();it++){
        Unit * unit[2];
        int counter = 0;
        for(vector<Parameter *>::iterator paramIt = this->parameter->begin(); paramIt!= this->parameter->end(); paramIt++){
            unit[counter]= (**it)[(*paramIt)->getName()]->getUnit();
        }
        if(!nearPoint(unit[0]->pt, unit[1]->pt)){
            it=resultTable->erase(it);
        }else{
            it++;
        }
    }
}
int Nearest::calcDist(Point a, Point b){
    int retPoint=0;
    if(a.x > b.x)
        retPoint+=a.x-b.x;
    else
        retPoint+=b.x-a.x;
    if(a.y > b.y)
        retPoint+=a.y-b.y;
    else
        retPoint+=b.y-a.y;
    return retPoint;
}

void Nearest::execute(){
    char * param[2];
    int counter = 0;
    for(vector<Parameter *>::iterator it= parameter->begin(); it!=parameter->end();it++){
        param[counter]=(*it)->getName();
		counter++;
        
    }
    vector<unordered_map<string ,DataCell *> *> * resultTable= result->getTable();
    for(vector<unordered_map<string ,DataCell *> *>::iterator dataIt= resultTable->begin();dataIt!=resultTable->end();dataIt++){
        Unit *self = (**dataIt)[param[0]]->getUnit();
        vector<int> enemyList;
        int shortestDist=9999;
        Unit shortestUnit;
        this->engine->getAllUnits(R_ENEMY, enemyList);
        for(vector<int>::iterator unitIt = enemyList.begin();unitIt!= enemyList.end();unitIt++){
            Unit tempUnit=engine->getUnitBy(R_ENEMY, (*unitIt));
            int dist= calcDist(self->pt, tempUnit.pt);
            if(dist<shortestDist){
                shortestUnit = tempUnit;
				shortestDist=dist;
            }
        }
		Unit * insertUnit= new Unit;
		*insertUnit= shortestUnit;
        (**dataIt)[param[1]]= new DataCell(insertUnit);
    }
}

void UnitToUnitDirection::execute(){
	char * param[3];
    int counter = 0;
    for(vector<Parameter *>::iterator it= parameter->begin(); it!=parameter->end();it++){
        param[counter] = new char [20];
        strcpy(param[counter], (*it)->getName());
        counter++;
    }
	vector<unordered_map<string ,DataCell *> *> * resultTable= result->getTable();
	for(vector<unordered_map<string ,DataCell *> *>::iterator it= resultTable->begin();it!=resultTable->end();){
		Point from = (**it)[param[0]]->getUnit()->pt;
		Point to = (**it)[param[1]]->getUnit()->pt;
		DIRECTION dest= getDirection(from, to);
		if((**it)[param[2]]->getDirection()!=dest){
			it=resultTable->erase(it);
		}else{
			it++;
		}
	}
}

DIRECTION UnitToUnitDirection::getDirection(Point from, Point to){
	if(from.y==to.y){
		if(from.x<to.x)
			return D_EAST;
		else
			return D_WEST;
	}
	else{
		if(from.y<to.y)
			return D_NORTH;
		else
			return D_SOUTH;
	}	
}

void UnitHP::execute(){
	char * param;
	Comparison comparisonType;
	int constNum;
    int counter = 0;
    for(vector<Parameter *>::iterator it= parameter->begin(); it!=parameter->end();it++){
		switch(counter) {
			case 0:
				param=new char[20];
				strcpy(param, (*it)->getName());
				break;
			case 1:
				comparisonType= (*it)->getComparison();
				break;
			case 2:
				constNum=(*it)->getConstNum();
				break;
		}
		counter++;
    }
	vector<unordered_map<string ,DataCell *> *> * resultTable= result->getTable();
	for(vector<unordered_map<string ,DataCell *> *>::iterator it= resultTable->begin();it!=resultTable->end();){
		bool del = 0;
		switch (comparisonType) {
			case _bigger:
				if(!((**it)[param]->getUnit()->health>constNum))
					del=1;
				break;
			case _biggerOrEqual:
				if(!((**it)[param]->getUnit()->health>=constNum))
					del=1;
				break;
			case _smaller:
				if(!((**it)[param]->getUnit()->health<constNum))
					del=1;
				break;
			case _smallerOrEqual:
				if(!((**it)[param]->getUnit()->health<=constNum))
					del=1;
				break;
			case _equal:
				if(!((**it)[param]->getUnit()->health==constNum))
					del=1;
				break;
		}
		
		if(del){
			it=resultTable->erase(it);
		}else
			it++;
	}
}