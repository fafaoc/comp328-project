//
//  Rule.h
//  COMP328Project
//
//  Created by FAFA on 13年5月7日.
//  Copyright (c) 2013年 FAFA. All rights reserved.
//

#ifndef __COMP328Project__Rule__
#define __COMP328Project__Rule__

#include <iostream>
#include "ruleHelper.h"

class Near:public Rule{
	public:
    Near(GameEngine * game, vector<Parameter *> * parameter): Rule(game, parameter){}
    void execute();
    bool nearPoint(Point a, Point b);
};

class Nearest:public Rule{
public:
    Nearest(GameEngine * game, vector<Parameter *> * parameter): Rule(game, parameter){}
    int calcDist(Point a, Point b);
    void execute();
    
};

class UnitToUnitDirection:public Rule{
	public:
	DIRECTION getDirection(Point from , Point to);
    UnitToUnitDirection(GameEngine * game, vector<Parameter *> * parameter): Rule(game, parameter){}
    void execute();
};


class UnitHP:public Rule{
public:
    UnitHP(GameEngine * game, vector<Parameter *> * parameter): Rule(game, parameter){}
    void execute();
};

#endif /* defined(__COMP328Project__Rule__) */
